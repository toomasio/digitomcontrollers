﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace DigitomControllers
{
    public class CinemachineSpawnCamera : MonoBehaviour
    {
        [SerializeField] protected CinemachineVirtualCameraBase cameraToSpawn;
        [SerializeField] protected Transform followTarget;
        [SerializeField] protected Transform lookAtTarget;
        [SerializeField] protected bool spawnOnStart;

        private void Start()
        {
            if (spawnOnStart)
                SpawnInCamera();
        }

        public void SpawnInCamera()
        {
            var spawn = Instantiate(cameraToSpawn);
            spawn.Follow = followTarget;
            spawn.LookAt = lookAtTarget;
        }
    }
}



﻿using UnityEngine;
using Cinemachine;
using UnityEngine.InputSystem;

public class CinemachineInputSystemLook : MonoBehaviour
{
    [SerializeField] private InputActionReference lookInput = null;
    [SerializeField] private float lookSens = 1;

    private InputAction lookAction;
    private Vector2 look;

    void Awake()
    {    
        if (lookInput != null) lookAction = lookInput.action;
        if (lookAction != null)
        {
            lookAction.Enable();
            lookAction.performed += ctx =>
            {
                look = ctx.ReadValue<Vector2>() * lookSens;
                CinemachineCore.GetInputAxis = GetAxisCustom;
            };
        }
    }

    public float GetAxisCustom(string axisName)
    {
        if (axisName == "Mouse X")
        {
            return look.x;
        }
        else if (axisName == "Mouse Y")
        {
            return look.y;
        }
        return 0;
    }

    private void OnEnable()
    {
        lookAction?.Enable();
    }

    private void OnDisable()
    {
        lookAction?.Disable();
    }
}

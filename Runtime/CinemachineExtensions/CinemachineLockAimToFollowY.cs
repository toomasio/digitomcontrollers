﻿using UnityEngine;
using Cinemachine;

public class CinemachineLockAimToFollowY : CinemachineExtension
{
    protected override void PostPipelineStageCallback(CinemachineVirtualCameraBase vcam, CinemachineCore.Stage stage, ref CameraState state, float deltaTime)
    {
        if (vcam.Follow)
        {
            if (stage == CinemachineCore.Stage.Aim)
            {
                var followRot = vcam.Follow.rotation.eulerAngles;
                var camRot = state.RawOrientation.eulerAngles;
                state.RawOrientation = Quaternion.Euler(new Vector3(camRot.x, followRot.y, camRot.z));
            }
        }

    }
}
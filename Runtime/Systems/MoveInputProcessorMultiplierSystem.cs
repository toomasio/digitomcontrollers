using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomControllers
{
    public class MoveInputProcessorMultiplierSystem : IToggleable, IContainInputOutput<Vector3>, ISyncData<MoveInputProcessorMultiplierData>
    {
        private Vector3 input;
        public Vector3 Input { set => input = value; }
        public Vector3 Output { get; protected set; }
        public bool Enabled { get; set; }

        private MoveInputProcessorMultiplierData data;

        public MoveInputProcessorMultiplierSystem(MoveInputProcessorMultiplierData data)
        {
            this.data = data;
        }

        public void Sync(MoveInputProcessorMultiplierData data)
        {
            this.data = data;
        }

        public void Tick()
        {
            if (!Enabled) { Output = input; return; }
            Output = input * data.multiplier;
        }

        
    }
}



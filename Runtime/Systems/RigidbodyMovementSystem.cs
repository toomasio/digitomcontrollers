﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomControllers
{
    public class RigidbodyMovementSystem : MovementSystem
    {
        protected readonly Rigidbody rb;

        public RigidbodyMovementSystem(Transform _transform, Rigidbody rb, MovementSettings _movementSettings) 
            : base (_transform, _movementSettings) 
        {
            this.rb = rb;
        }

        public override void OnEnter()
        {
            base.OnEnter();
            rb.drag = settings.linearDrag;
        }

        public override void OnExit()
        {
            base.OnExit();
            rb.constraints = RigidbodyConstraints.FreezeRotation;
        }

        protected override void Move()
        {
            var force = CurDesiredVelocity * Time.deltaTime;
            rb.AddForce(force, ForceMode.VelocityChange);
        }

        protected override void LockPositionX()
        {
            rb.constraints = RigidbodyConstraints.FreezePositionX;
        }

        protected override void LockPositionY()
        {
            rb.constraints = RigidbodyConstraints.FreezePositionY;
        }

        protected override void LockPositionZ()
        {
            rb.constraints = RigidbodyConstraints.FreezePositionZ;
        }

    }
}
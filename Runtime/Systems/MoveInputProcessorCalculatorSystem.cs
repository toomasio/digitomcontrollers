using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomControllers
{
    public class MoveInputProcessorCalculatorSystem : IContainOutput<Vector3>
    {
        public Vector3 Output { get; protected set; }

        private List<IContainInputOutput<Vector3>> inputProcessors;

        public void AddProcessor(IContainInputOutput<Vector3> processor)
        {
            if (inputProcessors == null) inputProcessors = new List<IContainInputOutput<Vector3>>();
                inputProcessors.Add(processor);
        }

        public void Tick()
        {
            for (int i = 0; i < inputProcessors.Count; i++)
            {
                if (i != 0) inputProcessors[i].Input = Output;
                Output = inputProcessors[i].Output;
            }
        }

    }
}



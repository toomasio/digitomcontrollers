﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomControllers
{
    public class TransformLookSystem : ILookState
    {
        private readonly Transform transform;
        private readonly LookSettings settings;

        public Vector2 DirectionInput { get; set; }
        public float SpeedMultiplier { get; set; }

        protected Vector3 lookDirection;
        protected Vector3 tarPos;
        protected Vector3 tarDir;
        protected Quaternion tarRot;

        public TransformLookSystem(Transform transform, LookSettings lookSettings)
        {
            this.transform = transform;
            settings = lookSettings;
        }

        public virtual void OnEnter() { }

        public void OnExit() { }

        public virtual void Tick()
        {
            LookTransform();
        }

        public void FixedTick() { }

        public void LateTick() { }

        protected virtual Vector3 ConvertInputToLookDirection(Vector2 _input)
        {
            var convertedMove = new Vector3();
            //x input
            if (settings.inputXTo == AxisType.X)
                convertedMove = new Vector3(DirectionInput.x, convertedMove.y, convertedMove.z);
            else if (settings.inputXTo == AxisType.Y)
                convertedMove = new Vector3(convertedMove.x, DirectionInput.x, convertedMove.z);
            else if (settings.inputXTo == AxisType.Z)
                convertedMove = new Vector3(convertedMove.x, convertedMove.y, DirectionInput.x);

            //y input
            if (settings.inputYTo == AxisType.X)
                convertedMove = new Vector3(DirectionInput.y, convertedMove.y, convertedMove.z);
            else if (settings.inputYTo == AxisType.Y)
                convertedMove = new Vector3(convertedMove.x, DirectionInput.y, convertedMove.z);
            else if (settings.inputYTo == AxisType.Z)
                convertedMove = new Vector3(convertedMove.x, convertedMove.y, DirectionInput.y);

            return convertedMove.normalized;
        }

        protected virtual void LookTransform()
        {
            lookDirection = ConvertInputToLookDirection(DirectionInput);

            if (lookDirection == Vector3.zero)return;

            tarPos = transform.position + lookDirection;
            tarDir = (tarPos - transform.position).normalized;

            if (settings.forwardAxis == AxisType.X)
                tarRot = XLookRotation(tarDir);
            else if (settings.forwardAxis == AxisType.Y)
                tarRot = YLookRotation(tarDir);
            else
                tarRot = Quaternion.LookRotation(tarDir);

            if (settings.rotateSpeed > 0)
                transform.rotation = Quaternion.Slerp(transform.rotation, tarRot, settings.rotateSpeed * SpeedMultiplier * Time.deltaTime);
            else
                transform.rotation = tarRot;
        }

        Quaternion XLookRotation(Vector3 right, Vector3 up = default)
        {
            if (up == default)
                up = Vector3.up;

            Quaternion rightToForward = Quaternion.Euler(0, -90, 0);
            Quaternion forwardToTarget = Quaternion.LookRotation(right, up);

            return forwardToTarget * rightToForward;
        }

        Quaternion YLookRotation(Vector3 right, Vector3 up = default)
        {
            if (up == default)
                up = Vector3.up;

            Quaternion upToForward = Quaternion.Euler(0, 90, -90);
            Quaternion forwardToTarget = Quaternion.LookRotation(right, up);

            return forwardToTarget * upToForward;
        }
    }
}
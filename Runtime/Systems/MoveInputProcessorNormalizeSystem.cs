using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomControllers
{
    public class MoveInputProcessorNormalizeSystem : IContainInputOutput<Vector3>, ISyncData<MoveInputProcessorNormalizeData>
    {
        private Vector3 input;
        public Vector3 Input { set => input = value; }
        public Vector3 Output { get; protected set; }

        private MoveInputProcessorNormalizeData data;

        public void Sync(MoveInputProcessorNormalizeData data)
        {
            this.data = data;
        }

        public void Tick()
        {
            if (data.normalize)
                Output = input.normalized;
        }

    }
}



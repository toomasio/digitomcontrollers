using DigitomUtilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomControllers
{
    public class MoveInputProcessorCrouchSystem : IToggleable, IContainInputOutput<Vector3>
    {
        private Vector3 input;
        public Vector3 Input { set => input = value; }
        public Vector3 Output { get; protected set; }
        public bool Enabled { get; set; }

        private MoveInputProcessorCrouchData data;

        private float startSpeed;
        private float targetSpeed;
        private bool triggered;
        private Timer timer;

        public MoveInputProcessorCrouchSystem(MoveInputProcessorCrouchData data)
        {
            this.data = data;
        }

        public void Tick()
        {
            Output = input;
            if (!data.enable) return;
            if (Enabled)
            {
                if (!triggered)
                {
                    triggered = true;
                    startSpeed = input.magnitude;
                    targetSpeed = data.multiplier;
                    var time = Mathf.Abs(targetSpeed - startSpeed) / data.transitionSpeed;
                    timer = new Timer(time);
                }
                timer.Tick();
                Output = input * Mathf.Lerp(startSpeed, targetSpeed, timer.Perc);
            }
            else if (triggered)
                triggered = false;

        }

    }
}



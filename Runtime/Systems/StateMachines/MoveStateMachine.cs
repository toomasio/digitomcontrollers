﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DigitomControllers
{
    public class MoveStateMachine : StateMachine<IMoveState>
    {
        public float SpeedMultiplier { get; set; }
        public Vector2 DirectionInput { get; set; }
        public Vector3 GroundNormal { get; set; }

        public override void Tick()
        {
            if (curState != null)
            {
                curState.SpeedMultiplier = SpeedMultiplier;
                curState.DirectionInput = DirectionInput;
                curState.GroundNormal = GroundNormal;
            }
            base.Tick();
        }

        public override void SetState(IMoveState _state)
        {
            if (curState != null)
                _state.InitialVelocity = curState.CurDesiredVelocity;
            base.SetState(_state);
        }
        
        public void DrawGizmos()
        {
            curState?.DrawGizmos();
        }
    }
}



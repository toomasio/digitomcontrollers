﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DigitomControllers
{
    public class StateMachine<T> where T : IState
    {
        protected readonly List<Transition> transitions = new List<Transition>();
        protected readonly List<StateTransition> stateTransitions = new List<StateTransition>();
        public T CurState => curState;
        protected T curState;
        protected StateTransition[] curStateTransitions;
        protected Transition curTransition;

        public virtual void Tick()
        {
            CheckTransitions();
            curState?.Tick();
        }

        void CheckTransitions()
        {
            curTransition = GetTransition();
            if (curTransition == null)return;
            if (curTransition.NextState == null)return;
            if (!curTransition.NextState.Equals(curState))
                SetState(curTransition.NextState);
        }

        public virtual void FixedTick()
        {
            curState?.FixedTick();
        }

        public virtual void LateTick()
        {
            curState?.LateTick();
        }

        public virtual void SetState(T _state)
        {
            curState?.OnExit();
            curState = _state;
            curState?.OnEnter();
            curStateTransitions = stateTransitions
                .Where(x => x.PrevState.Equals(curState))
                .ToArray();
        }

        public virtual void AddTransition(T _nextState, Func<bool> _condition)
        {
            transitions.Add(new Transition(_nextState, _condition));
        }

        public virtual void AddStateTransition(T _prevState, T _nextState, Func<bool> _condition)
        {
            stateTransitions.Add(new StateTransition(_prevState, _nextState, _condition));
        }

        private Transition GetTransition()
        {
            for (int i = 0; i < transitions.Count; i++)
                if (transitions[i].Condition())return transitions[i];

            for (int i = 0; i < curStateTransitions.Length; i++)
                if (curStateTransitions[i].Condition())return curStateTransitions[i];

            return null;
        }

        protected class Transition
        {
            public Func<bool> Condition { get; }
            public T NextState { get; }

            public Transition(T _nextState, Func<bool> _condition)
            {
                Condition = _condition;
                NextState = _nextState;
            }
        }

        protected class StateTransition : Transition
        {
            public T PrevState { get; }

            public StateTransition(T _prevState, T _nextState, Func<bool> _condition) : base(_nextState, _condition)
            {
                PrevState = _prevState;
            }
        }
    }
}
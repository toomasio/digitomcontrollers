﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DigitomControllers
{
    public class LookStateMachine : StateMachine<ILookState>
    {
        public float SpeedMultiplier { get; set; }
        public Vector2 DirectionInput { get; set; }

        public override void Tick()
        {
            if (curState != null)
            {
                curState.SpeedMultiplier = SpeedMultiplier;
                curState.DirectionInput = DirectionInput;
            }
            base.Tick();
        }

    }
}
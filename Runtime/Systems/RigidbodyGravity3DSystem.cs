using DigitomPhysics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomControllers
{
    public class RigidbodyGravity3DSystem : IToggleable, ITickable
    {
        public bool Enabled { get; set; }

        private readonly RigidbodyGravity3DSettings settings;
        private readonly Rigidbody rb;
        private readonly IPhysicsDetectable3D grounded;

        private bool velReset;

        public RigidbodyGravity3DSystem(RigidbodyGravity3DSettings settings, Rigidbody rb, IPhysicsDetectable3D grounded)
        {
            this.settings = settings;
            this.rb = rb;
            this.grounded = grounded;
            Enabled = true;
        }

        public void Tick()
        {
            if (!settings.enable) return;
            if (!Enabled) return;
            if (!grounded.Detected)
            {
                velReset = false;
                rb.AddForce(Vector3.down * settings.gravity, settings.forceMode);
            }
            else if (!velReset)
            {
                velReset = true;
                rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
            }
            
        }

    }
}



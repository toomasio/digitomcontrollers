using DigitomPhysics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomControllers
{
    public class MoveInputProcessorSlopeSystem : IContainInputOutput<Vector3>, ISyncData<MoveInputProcessorSlopeData>
    {
        private Vector3 input;
        public Vector3 Input { set => input = value; }
        public Vector3 Output { get; protected set; }

        private Transform transform;
        private MoveInputProcessorSlopeData data;
        private IPhysicsDetectable3D grounded;

        public MoveInputProcessorSlopeSystem(Transform transform, IPhysicsDetectable3D grounded, MoveInputProcessorSlopeData data)
        {
            this.transform = transform;
            this.grounded = grounded;
            this.data = data;
        }

        public void Sync(MoveInputProcessorSlopeData data)
        {
            this.data = data;
        }

        public void Tick()
        {
            if (!data.enable)
            {
                Output = input;
                return;
            }
            var convertedMove = input;
            if (grounded.Detected && input != default)
            {
                //need to find the closest hit position relative to input direction...then pick that hitnormal
                //this should prevent the player getting stuck when colliding with multiple surfaces
                float highestDot = 0;
                var closestNorm = grounded.HitNormal;
                for (int i = 0; i < grounded.HitInfos.Length; i++)
                {
                    var pos = grounded.HitInfos[i].hitPoint;
                    var norm = grounded.HitInfos[i].hitNormal;
                    var dir = (pos - transform.position);
                    dir.y = 0;
                    dir = dir.normalized;
                    var angle = Vector3.Dot(input.normalized, dir);
                    if (angle > highestDot)
                    {
                        closestNorm = norm;
                        highestDot = angle;
                    }

                }
                convertedMove = Vector3.ProjectOnPlane(convertedMove, closestNorm);

                if (data.enableMaxSlope)
                {
                    var angle = Vector3.Angle(Vector3.up, closestNorm);

                    if (angle > data.maxSlopeAngle && convertedMove.y > 0)
                    {
                        convertedMove = Vector3.zero;
                    }
                }

            }
            Output = convertedMove;
        }

    }
}



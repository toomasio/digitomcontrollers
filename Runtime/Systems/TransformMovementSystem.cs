﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomControllers
{
    public class TransformMovementSystem : MovementSystem
    {
        public TransformMovementSystem(Transform _transform, MovementSettings _movementSettings) 
            : base (_transform, _movementSettings) { }

        protected override void Move()
        {
            transform.position += CurDesiredVelocity * Time.deltaTime;
        }

        protected override void LockPositionX()
        {
            transform.position = new Vector3(startPos.x, transform.position.y, transform.position.z);
        }

        protected override void LockPositionY()
        {
            transform.position = new Vector3(transform.position.x, startPos.y, transform.position.z);
        }

        protected override void LockPositionZ()
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, startPos.z);
        }
    }
}
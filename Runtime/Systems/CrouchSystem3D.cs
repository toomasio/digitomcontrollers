﻿using UnityEngine;
using DigitomUtilities;
using DigitomPhysics;
using System;

namespace DigitomControllers
{
    public class CrouchSystem3D : ITickable
    {
        public enum BodyState { Transitioning, Standing, Crouching, Proneing }
        public BodyState State { get; private set; }
        public BodyState PrevState { get; private set; }
        public BodyState NextState { get; private set; }
        private CrouchSettings crouchSettings;
        private IPhysicsDetectable3D grounded;
        private Transform root;
        private CapsuleCollider collider;

        private bool triggered;
        private float startColliderRadius;
        private float startColliderHeight;
        private Vector3 startColliderCenter;
        private Vector3 startCameraPos;
        private Vector3 targetColliderCenter;
        private float targetColliderRadius;
        private float targetColliderHeight;
        private int targetColliderDirection;
        private Vector3 targetCameraPos;
        private BodyState targetBodyState;
        private Timer timer;

        public CrouchSystem3D(IPhysicsDetectable3D grounded, CrouchSettings settings, Transform root, CapsuleCollider collider)
        {
            this.crouchSettings = settings;
            this.grounded = grounded;
            this.root = root;
            this.collider = collider;
            State = BodyState.Standing;
            NextState = BodyState.Standing;
        }

        public void Tick()
        {
            if (!crouchSettings.enable) return;
            if (!triggered) return;

            timer.Tick();
            collider.radius = Mathf.Lerp(startColliderRadius, targetColliderRadius, timer.Perc);
            collider.height = Mathf.Lerp(startColliderHeight, targetColliderHeight, timer.Perc);
            collider.center = Vector3.Lerp(startColliderCenter, targetColliderCenter, timer.Perc);
            if (crouchSettings.cameraPos)
                crouchSettings.cameraPos.localPosition = Vector3.Lerp(startCameraPos, targetCameraPos, timer.Perc);

            if (timer.Perc >= 1)
            {
                State = targetBodyState;
                collider.direction = targetColliderDirection;
                triggered = false;
            }
        }

        public void Stand()
        {
            if (!crouchSettings.enable) return;
            if (NextState == BodyState.Standing) return;
            ResetCrouchTimer(crouchSettings.standingPosition, BodyState.Standing, crouchSettings.standingCameraLocalPos);
        }

        public void Crouch()
        {
            if (!crouchSettings.enable) return;
            if (NextState == BodyState.Crouching) return;
            ResetCrouchTimer(crouchSettings.crouchPosition, BodyState.Crouching, crouchSettings.crouchCameraLocalPos);
            if (PrevState == BodyState.Proneing)
                collider.direction = (int)crouchSettings.pronePosition.direction;
        }

        public void Prone()
        {
            if (!crouchSettings.enable) return;
            if (!crouchSettings.enableProne) return;
            if (NextState == BodyState.Proneing) return;
            ResetCrouchTimer(crouchSettings.pronePosition, BodyState.Proneing, crouchSettings.proneCameraLocalPos);
            if (PrevState == BodyState.Crouching)
                collider.direction = (int)crouchSettings.pronePosition.direction;
        }

        void ResetCrouchTimer(CapsuleSettings capsuleSettings, BodyState bodyState, Vector3 targetCamPos)
        {
            triggered = true;
            startColliderRadius = collider.radius;
            startColliderHeight = collider.height;
            targetColliderRadius = capsuleSettings.radius;
            targetColliderHeight = capsuleSettings.height;
            startColliderCenter = collider.center;
            targetColliderCenter = capsuleSettings.center;
            targetColliderDirection = (int)capsuleSettings.direction;
            if (crouchSettings.cameraPos)
                startCameraPos = crouchSettings.cameraPos.localPosition;
            targetCameraPos = targetCamPos;
            var time = Mathf.Abs(targetColliderHeight - collider.height) / crouchSettings.crouchTransitionSpeed;
            timer = new Timer(time);
            targetBodyState = bodyState;

            PrevState = NextState;
            State = BodyState.Transitioning;
            NextState = targetBodyState;
        }
    }
}



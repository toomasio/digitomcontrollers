using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomControllers
{
    public class PlatformStickSystem : ITickable, IToggleable
    {
        private readonly PlatformStickSettings settings;
        private readonly Transform transform;
        private readonly Rigidbody rb;
        private readonly DetectGrounded grounded;

        private Transform curPlatform;
        private Vector3 lastPos;

        public PlatformStickSystem(PlatformStickSettings settings, DetectGrounded grounded)
        {
            this.settings = settings;
            this.grounded = grounded;
            this.transform = grounded.transform;
            this.rb = grounded.GetComponent<Rigidbody>();
            Enabled = true;
        }

        public bool Enabled { get; set; }

        public void Tick()
        {
            if (!settings.enable) return;
            if (!Enabled) return;

            curPlatform = null;
            var hits = grounded.GroundDetection.HitInfos;
            if (hits != null)
            {
                for (int i = 0; i < hits.Length; i++)
                {
                    var col = hits[i].collider;
                    if (col == null) continue;
                    var stick = col.GetComponent<StickyPlatform>();
                    if (stick != null)
                    {
                        curPlatform = stick.transform;
                    }

                }
            }
            
            if (curPlatform == null)
            {
                lastPos = default;
                if (settings.IsSticking)
                {
                    settings.IsSticking = false;
                    if (settings.stickType == StickType.PlatformAsParent)
                        transform.SetParent(null);
                }
                return;
            }

            //position difference
            if (lastPos == default) lastPos = curPlatform.position;
            var diff = curPlatform.position - lastPos;
            lastPos = curPlatform.position;

            if (!settings.IsSticking)
            {
                if (settings.stickType == StickType.PlatformAsParent)
                    transform.SetParent(curPlatform);
                settings.IsSticking = true;
            }

            
            if (settings.stickType == StickType.TransformDiff)
                transform.position += diff;
            else if (settings.stickType == StickType.RigidbodyDiff)
                rb.position += diff;

            
            
        }

    }
}



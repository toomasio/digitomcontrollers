﻿using UnityEngine;
using DigitomUtilities;
using DigitomPhysics;

namespace DigitomControllers
{
    public class JumpSystem3D : ITriggerable
    {
        private Rigidbody rb;
        private JumpSettings jumpSettings;
        private IPhysicsDetectable3D grounded;

        private int currentAirJumps;

        public bool ActionValidated { get; private set; }

        public JumpSystem3D(Rigidbody rb, IPhysicsDetectable3D grounded, JumpSettings settings)
        {
            this.rb = rb;
            this.grounded = grounded;
            this.jumpSettings = settings;
            jumpSettings.currentJumpForce = jumpSettings.defaultJumpForce;
        }

        public void OnTriggered()
        {
            ActionValidated = false;
            if (!jumpSettings.enable) return;
            if (!grounded.Detected)
            {
                if (currentAirJumps >= jumpSettings.maxAirJumps)
                    return;
                else
                    currentAirJumps++;
            }
            else
                currentAirJumps = 0;

            if (jumpSettings.consistentForce)
                rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);

            ActionValidated = true;
            rb.AddForce(Vector3.up * jumpSettings.currentJumpForce, jumpSettings.forceMode);
        }
    }
}



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitomPhysics;

namespace DigitomControllers
{
    public class MoveInputProcessorMapperSystem : IContainInputOutput<Vector3>, ISyncData<MoveInputProcessorMappingData>
    {
        private Vector3 input;
        public Vector3 Input { set => input = value; }
        public Vector3 Output { get; protected set; }

        private MoveInputProcessorMappingData data;

        public MoveInputProcessorMapperSystem(MoveInputProcessorMappingData data)
        {
            this.data = data;
        }

        public void Sync(MoveInputProcessorMappingData data)
        {
            this.data = data;
        }

        public void Tick()
        {
            var convertedMove = input;
            //x input
            if (data.mapXTo == AxisType.X)
                convertedMove = new Vector3(input.x, convertedMove.y, convertedMove.z);
            else if (data.mapXTo == AxisType.Y)
                convertedMove = new Vector3(0, input.x, convertedMove.z);
            else if (data.mapXTo == AxisType.Z)
                convertedMove = new Vector3(0, convertedMove.y, input.x);

            //y input
            if (data.mapYTo == AxisType.X)
                convertedMove = new Vector3(input.y, 0, convertedMove.z);
            else if (data.mapYTo == AxisType.Y)
                convertedMove = new Vector3(convertedMove.x, input.y, convertedMove.z);
            else if (data.mapYTo == AxisType.Z)
                convertedMove = new Vector3(convertedMove.x, 0, input.y);
            
            //z input
            if (data.mapZTo == AxisType.X)
                convertedMove = new Vector3(input.z, convertedMove.y, 0);
            else if (data.mapZTo == AxisType.Y)
                convertedMove = new Vector3(convertedMove.x, input.z, 0);
            else if (data.mapZTo == AxisType.Z)
                convertedMove = new Vector3(convertedMove.x, convertedMove.y, input.z);

            Output = convertedMove;
            
        }

    }
}



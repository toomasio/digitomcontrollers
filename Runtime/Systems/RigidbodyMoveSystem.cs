using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitomUtilities;

namespace DigitomControllers
{
    public class RigidbodyMoveSystem : ITickable, ISyncData<RigidbodyMoveData>
    {
        public Vector3 Input { get; set; }

        private readonly Rigidbody rb;
        private RigidbodyMoveData data;

        public RigidbodyMoveSystem(Rigidbody rb, RigidbodyMoveData data)
        {
            this.rb = rb;
            this.data = data;
        }

        public void Sync(RigidbodyMoveData data)
        {
            this.data = data;
        }

        public void Tick()
        {
            if (Input.IsNaN()) return;
            rb.AddForce(Input, data.forceMode);
        }

    }
}



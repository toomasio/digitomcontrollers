﻿using UnityEngine;
using DigitomUtilities;
using DigitomPhysics;

namespace DigitomControllers
{
    public class SlopeSystem
    {
        private readonly SlopeSettings settings;
        private readonly Transform transform;
        private float angle;
        private Vector3 inputPoint;
        private bool groundHit;
        private Ray ray;
        private RaycastHit hit;
        private float slopeSpeedMultiplier = 1;

        public SlopeSystem(Transform _transform, SlopeSettings _settings)
        {
            transform = _transform;
            settings = _settings;
        }

        public Vector3 GetAdjustedMoveInput(Vector3 _moveInput)
        {
            //create position point based on input
            inputPoint = transform.position + _moveInput;
            ray = new Ray(transform.position + transform.up * settings.height, -transform.up);
            groundHit = Physics.Raycast(ray, out hit, settings.distance, settings.groundMask);
            if (groundHit)
            {
                var plane = Vector3.ProjectOnPlane(_moveInput, hit.normal);
                inputPoint = transform.position + plane;
                return plane;
            }
            else
                return _moveInput;

        }

        public float GetSpeed(Vector3 _moveInput)
        {
            //slow player down on slopes
            angle = Vector3.Angle(Vector3.up, hit.normal);
            if (angle > settings.maxAngle)
            {
                //if input is moving towards the slope...slow player down
                if (_moveInput.y >= 0)
                {
                    slopeSpeedMultiplier -= Time.deltaTime;
                }
                else
                    slopeSpeedMultiplier += Time.deltaTime;

            }
            else
                slopeSpeedMultiplier += Time.deltaTime;

            slopeSpeedMultiplier = Mathf.Clamp(slopeSpeedMultiplier, settings.minSpeed, 1);

            return slopeSpeedMultiplier;

        }

        public void DrawGizmos()
        {
            Gizmos.DrawLine(ray.origin, ray.origin + (ray.direction * settings.distance));
            if (!groundHit) return;
            Gizmos.color = Color.cyan;
            
            Gizmos.DrawLine(hit.point, inputPoint);
            Gizmos.DrawLine(transform.position, inputPoint);
            Gizmos.DrawSphere(inputPoint, 0.1f);
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(hit.point, 0.1f);

        }
    }
}



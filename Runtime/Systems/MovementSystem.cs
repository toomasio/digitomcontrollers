﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomControllers
{
    public abstract class MovementSystem : IMoveState
    {
        protected readonly Transform transform;
        protected readonly MovementSettings settings;

        public Vector2 DirectionInput { get; set; }
        public Vector3 GroundNormal { get; set; }
        public float SpeedMultiplier { get; set; }

        public Vector3 InitialVelocity { get; set; }
        public Vector3 CurDesiredVelocity { get; protected set; }
        public Vector3 CurVelocity { get; protected set; }
        public float CurVelocitySpeed { get; protected set; }
        protected float curSpeed;
        protected Vector3 desiredVelocity;
        protected SlopeSystem slopeSystem;
        protected Camera cam;
        protected Vector3 startPos;
        protected Vector3 lastPos;
        protected abstract void Move();
        protected abstract void LockPositionX();
        protected abstract void LockPositionY();
        protected abstract void LockPositionZ();

        public MovementSystem(Transform transform, MovementSettings movementSettings)
        {
            this.transform = transform;
            this.settings = movementSettings;
            curSpeed = settings.defaultSpeed;
            if (settings.slopeSettings.enable)
                slopeSystem = new SlopeSystem(transform, settings.slopeSettings);
        }

        public virtual void OnEnter()
        {
            startPos = transform.position;
            lastPos = startPos;
            CurDesiredVelocity = InitialVelocity;
            if (settings.holdPositionLock == StateMode.OnEnter)
                LockPosition();
        }

        public virtual void OnExit() 
        {
            if (settings.holdPositionLock == StateMode.OnExit)
                LockPosition();
        }

        public virtual void Tick()
        {
            var inputMove = ConvertInputToTranslation(DirectionInput);
            if (slopeSystem != null)
            {
                inputMove = slopeSystem.GetAdjustedMoveInput(inputMove);
                inputMove *= slopeSystem.GetSpeed(DirectionInput);
            }

            desiredVelocity = inputMove * SpeedMultiplier * curSpeed;

            if (settings.moveSensitivity != 0)
                CurDesiredVelocity = Vector3.Lerp(CurVelocity, desiredVelocity, Time.deltaTime * settings.moveSensitivity);
            else
                CurDesiredVelocity = desiredVelocity;

        }

        public void FixedTick()
        {
            Move();
            CalculateVelocity();
            if (settings.holdPositionLock == StateMode.OnFixedTick)
                LockPosition();
        }

        public void LateTick() 
        {
            if (settings.holdPositionLock == StateMode.OnLateTick)
                LockPosition();
        }

        protected virtual Vector3 ConvertInputToTranslation(Vector2 _input)
        {
            var convertedMove = new Vector3();
            //x input
            if (settings.inputXTo == AxisType.X)
                convertedMove = new Vector3(DirectionInput.x, convertedMove.y, convertedMove.z);
            else if (settings.inputXTo == AxisType.Y)
                convertedMove = new Vector3(convertedMove.x, DirectionInput.x, convertedMove.z);
            else if (settings.inputXTo == AxisType.Z)
                convertedMove = new Vector3(convertedMove.x, convertedMove.y, DirectionInput.x);

            //y input
            if (settings.inputYTo == AxisType.X)
                convertedMove = new Vector3(DirectionInput.y, convertedMove.y, convertedMove.z);
            else if (settings.inputYTo == AxisType.Y)
                convertedMove = new Vector3(convertedMove.x, DirectionInput.y, convertedMove.z);
            else if (settings.inputYTo == AxisType.Z)
                convertedMove = new Vector3(convertedMove.x, convertedMove.y, DirectionInput.y);

            //spacial direction
            if (settings.space == DirectionSpace.Self)
                convertedMove = transform.TransformDirection(convertedMove);
            else if (settings.space == DirectionSpace.Camera)
            {
                if (!cam) cam = Camera.main;
                convertedMove = cam.transform.TransformDirection(convertedMove);
                convertedMove = Vector3.ProjectOnPlane(convertedMove, transform.up);
            }

            //ignore certain axis?
            if (settings.flattenAxis == AxisType.X)
                convertedMove.x = 0;
            else if (settings.flattenAxis == AxisType.Y)
                convertedMove.y = 0;
            else if (settings.flattenAxis == AxisType.Z)
                convertedMove.z = 0;

            return convertedMove.normalized;
        }

        protected virtual void LockPosition()
        {
            if (settings.positionLock == AxisType.X)
                LockPositionX();
            else if (settings.positionLock == AxisType.Y)
                LockPositionY();
            else if (settings.positionLock == AxisType.Z)
                LockPositionZ();
        }

        protected virtual void CalculateVelocity()
        {
            CurVelocity = transform.position - lastPos;
            CurVelocitySpeed = Mathf.Round((CurVelocity / Time.deltaTime).magnitude * 100) / 100;
            lastPos = transform.position;
        }

        public virtual void DrawGizmos()
        {
            if (slopeSystem != null)
                slopeSystem.DrawGizmos();
        }
    }
}
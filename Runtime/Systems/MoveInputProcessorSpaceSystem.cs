using DigitomUtilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomControllers
{
    public class MoveInputProcessorSpaceSystem : IContainInputOutput<Vector3>, ISyncData<MoveInputProcessorSpaceData>
    {
        private Vector3 input;
        public Vector3 Input { set => input = value; }
        public Vector3 Output { get; protected set; }

        private Transform transform;
        private MoveInputProcessorSpaceData data;

        private Camera cam;

        public MoveInputProcessorSpaceSystem(Transform transform, MoveInputProcessorSpaceData data)
        {
            this.transform = transform;
            this.data = data;
        }

        public void Sync(MoveInputProcessorSpaceData data)
        {
            this.data = data;
        }

        public void Tick()
        {
            var convertedMove = input;

            //spacial direction
            if (data.space == DirectionSpace.Self)
                convertedMove = transform.TransformDirection(convertedMove);
            else if (data.space == DirectionSpace.Camera)
            {
                if (!cam) cam = Camera.main;
                var dir = transform.position - cam.transform.position;
                dir.y = 0;
                dir = dir.normalized;
                var matrix = Matrix4x4.TRS(transform.position, Quaternion.LookRotation(dir), Vector3.one);
                convertedMove = matrix.MultiplyVector(convertedMove);
            }

            Output = convertedMove;
        }

    }
}



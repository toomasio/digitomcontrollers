using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomControllers
{
    public class RigidbodyDragSystem : ITickable, IToggleable, ISyncData<RigidbodyDragData>
    {
        public bool Enabled { get; set; }

        private readonly Rigidbody rb;
        private RigidbodyDragData data;

        public RigidbodyDragSystem(Rigidbody rb, RigidbodyDragData data)
        {
            this.rb = rb;
            this.data = data;
        }

        public void Sync(RigidbodyDragData data)
        {
            this.data = data;
        }

        public void Tick()
        {
            if (!Enabled) return;
            rb.drag = data.drag;
        }

    }
}



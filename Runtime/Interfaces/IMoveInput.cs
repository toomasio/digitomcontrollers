﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomControllers
{
    public interface IMoveInput
    {
        Vector3 MoveInput { get; }
        event Action<Vector3> OnInputReceived;
    }
}



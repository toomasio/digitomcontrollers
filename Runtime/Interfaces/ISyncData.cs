﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomControllers
{
    public interface ISyncData<T>
    {
        void Sync(T data);
    }
}



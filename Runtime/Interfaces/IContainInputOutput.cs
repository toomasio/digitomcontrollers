﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomControllers
{
    public interface IContainInputOutput<T> : IContainInput<T>, IContainOutput<T>
    { 

    }
}



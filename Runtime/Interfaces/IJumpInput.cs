﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomControllers
{
    public interface IJumpInput
    {
        event Action OnInpPerformed;
        event Action OnInpCanceled;
    }
}



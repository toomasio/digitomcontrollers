﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomControllers
{
    public interface IToggleable
    {
        bool Enabled { get; set; }
    }
}



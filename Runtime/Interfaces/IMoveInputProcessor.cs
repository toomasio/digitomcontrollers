﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomControllers
{
    public interface IMoveInputProcessor
    {
        Vector3 InputDirection { get; set; }
        Vector3 OutputDirection { get; }
    }
}



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomControllers
{
    public interface IMoveState : IState
    {
        Vector2 DirectionInput { get; set; }
        Vector3 GroundNormal { get; set; }
        float SpeedMultiplier { get; set; }
        Vector3 InitialVelocity{ get; set; }
        Vector3 CurDesiredVelocity{ get; }
        void DrawGizmos();
        
    }
}



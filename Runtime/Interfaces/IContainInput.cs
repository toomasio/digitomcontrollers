﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomControllers
{
    public interface IContainInput<T>
    {
        T Input { set; }
    }
}



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomControllers
{
    public interface ILookState : IState
    {
        Vector2 DirectionInput { get; set; }
        float SpeedMultiplier { get; set; }

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomControllers
{
    public enum MoveAxisType { XToX, YToY, YToZ, XYToXZ, XYToXY }
    public enum AxisType { None, X, Y, Z }
}



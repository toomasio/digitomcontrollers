﻿using UnityEngine;
using DigitomUtilities;
using DigitomPhysics;

namespace DigitomControllers
{
    [System.Serializable]
    public class SlopeSettings
    {
        public bool enable = false;
        public float maxAngle = 45;
        public float height = 0.1f;
        public float distance = 0.3f;
        public float minSpeed = 0;
        public LayerMask groundMask = 0;
    }
}



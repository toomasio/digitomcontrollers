﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomControllers
{
    [System.Serializable]
    public class MoveInputProcessorNormalizeData : IMovementSettings
    {
        public bool normalize;
    }
}
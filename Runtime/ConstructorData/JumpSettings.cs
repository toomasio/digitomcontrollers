﻿using UnityEngine;
using DigitomUtilities;
using DigitomPhysics;

namespace DigitomControllers
{
    [System.Serializable]
    public class JumpSettings
    {
        public bool enable = false;
        public float defaultJumpForce = 5;
        public float currentJumpForce = 5;
        public int maxAirJumps = 0;
        public bool consistentForce = true;
        public ForceMode forceMode = ForceMode.VelocityChange;
    }
}



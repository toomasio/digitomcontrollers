﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomControllers
{
    [System.Serializable]
    public class RigidbodyDragData : IMovementSettings
    {
        public float drag = 0;
    }
}
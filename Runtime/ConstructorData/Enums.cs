﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomControllers
{
    public enum StateMode { None, OnEnter, OnExit, OnTick, OnFixedTick, OnLateTick }
    public enum VectorAxis { X, Y, Z }
}
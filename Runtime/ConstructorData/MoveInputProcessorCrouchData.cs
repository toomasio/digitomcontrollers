﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomControllers
{
    [System.Serializable]
    public class MoveInputProcessorCrouchData : IMovementSettings
    {
        public bool enable = true;
        public float multiplier = 1;
        public float transitionSpeed = 10;
    }
}
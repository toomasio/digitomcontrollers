﻿using UnityEngine;
using DigitomUtilities;
using DigitomPhysics;

namespace DigitomControllers
{
    [System.Serializable]
    public class CrouchSettings
    {
        public bool enable = false;
        public float crouchTransitionSpeed = 1;
        public CapsuleSettings standingPosition;
        public CapsuleSettings crouchPosition;
        public bool enableProne;
        public CapsuleSettings pronePosition;
        public Transform cameraPos;
        public Vector3 standingCameraLocalPos = Vector3.up;
        public Vector3 crouchCameraLocalPos = Vector3.up;
        public Vector3 proneCameraLocalPos = Vector3.up;
    }

    [System.Serializable]
    public class CapsuleSettings
    {
        public Vector3 center = Vector3.up;
        public float radius = 1;
        public float height = 2;
        public VectorAxis direction = VectorAxis.Y;
    }
}



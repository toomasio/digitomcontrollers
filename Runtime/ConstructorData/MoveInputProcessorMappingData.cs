﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomControllers
{
    [System.Serializable]
    public class MoveInputProcessorMappingData : IMovementSettings
    {
        public AxisType mapXTo = AxisType.X;
        public AxisType mapYTo = AxisType.Y;
        public AxisType mapZTo = AxisType.Z;
    }
}
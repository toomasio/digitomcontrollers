﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomControllers
{
    [System.Serializable]
    public class RigidbodyMoveSettings : IMovementSettings
    {
        public float defaultSpeed = 5;
        public float axialSpeedMultiplier = 1;
        public float latitudeSpeedMultiplier = 1;
        public float moveSensitivity = 0;
        public float linearDrag = 0;
        public AxisType inputXTo = AxisType.X;
        public AxisType inputYTo = AxisType.Z;
        public AxisType positionLock = AxisType.None;
        public StateMode holdPositionLock = StateMode.None;
        public AxisType flattenAxis = AxisType.None;
        public DirectionSpace space = DirectionSpace.Self;
        public SlopeSettings slopeSettings;
    }
}
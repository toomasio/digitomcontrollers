﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomControllers
{
    [System.Serializable]
    public class MoveInputProcessorMultiplierData : IMovementSettings
    {
        public float multiplier = 5;
    }
}
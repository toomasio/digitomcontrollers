﻿using UnityEngine;
using DigitomUtilities;
using DigitomPhysics;

namespace DigitomControllers
{
    public enum StickType { PlatformAsParent, TransformDiff, RigidbodyDiff }

    [System.Serializable]
    public class PlatformStickSettings
    {
        public bool enable = false;
        public StickType stickType = StickType.PlatformAsParent;
        public bool IsSticking { get; set; }
    }
}



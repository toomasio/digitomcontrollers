﻿using UnityEngine;
using DigitomUtilities;
using DigitomPhysics;

namespace DigitomControllers
{
    [System.Serializable]
    public class RigidbodyGravity3DSettings
    {
        public bool enable = false;
        public float gravity = 10;
        public ForceMode forceMode = ForceMode.Impulse;
    }
}



﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;
using DigitomPhysics;

namespace DigitomControllers
{
    [System.Serializable]
    public class MoveInputProcessorSlopeData : IMovementSettings
    {
        public bool enable = false;
        public bool enableMaxSlope = true;
        public float maxSlopeAngle = 45;
        public float height = 0.1f;
        public float distance = 0.3f;
        public float minSpeed = 0;
        public LayerMask groundMask = 0;
    }
}
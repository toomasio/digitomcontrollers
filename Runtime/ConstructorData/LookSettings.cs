﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomControllers
{
    [System.Serializable]
    public class LookSettings
    {
        public AxisType inputXTo = AxisType.X;
        public AxisType inputYTo = AxisType.Z;
        public AxisType forwardAxis = AxisType.Z;
        public float rotateSpeed = 0;
    }
}
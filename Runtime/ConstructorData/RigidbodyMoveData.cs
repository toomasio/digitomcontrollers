﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomControllers
{
    [System.Serializable]
    public class RigidbodyMoveData : IMovementSettings
    {
        public ForceMode forceMode = ForceMode.Impulse;
    }
}
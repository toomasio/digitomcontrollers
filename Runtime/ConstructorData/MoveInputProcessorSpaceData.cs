﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomControllers
{
    [System.Serializable]
    public class MoveInputProcessorSpaceData : IMovementSettings
    {
        public DirectionSpace space;
    }
}
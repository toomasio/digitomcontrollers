﻿using System;
using System.Collections;
using System.Collections.Generic;
using DigitomPhysics;
using UnityEngine;

namespace DigitomControllers
{
    [RequireComponent(typeof(Rigidbody), typeof(DetectGrounded), typeof(IJumpInput))]
    public class RigidbodyJump : MonoBehaviour
    {
        [SerializeField] protected float defaultJumpPower = 20;
        [SerializeField] protected float riseResistance = 5;
        [SerializeField] protected bool disableRiseResistanceOnJump = true;
        [SerializeField] protected float fallStartOnYVelocity = 0;
        [SerializeField] protected float fallMultiplier = 5;
        [SerializeField] protected int inAirJumps = 0;
        [SerializeField] protected ForceMode forceMode = default;
        
        public bool IsJumping { get; protected set; }
        protected int curInAirJumps = 0;
        protected float curJumpPower;
        protected float lastDefaultJumpPower;
        protected Rigidbody rb;
        protected IJumpInput input;
        protected DetectGrounded groundDetect;
        protected Action<Collider> GroundedHandler;
        protected Action<Collider> NotGroundedHandler;
        
        protected void Awake()
        {
            rb = GetComponent<Rigidbody>();
            input = GetComponent<IJumpInput>();
            groundDetect = GetComponent<DetectGrounded>();
            curJumpPower = defaultJumpPower;
            lastDefaultJumpPower = defaultJumpPower;
            GroundedHandler = (col) => 
            {
                SleepVertical();
                curInAirJumps = 0; 
                StopJump(); 
            };
            NotGroundedHandler = (col) => { };
        }

        private void OnEnable()
        {
            input.OnInpPerformed += Jump;
            input.OnInpCanceled += StopJump;
            groundDetect.OnGrounded += GroundedHandler;
            groundDetect.OnNotGrounded += NotGroundedHandler;
        }

        private void OnDisable()
        {
            input.OnInpCanceled -= StopJump;
            input.OnInpPerformed -= Jump;
            groundDetect.OnGrounded -= GroundedHandler;
            groundDetect.OnNotGrounded -= NotGroundedHandler;
        }

        private void Update()
        {
            if (defaultJumpPower != lastDefaultJumpPower)
            {
                curJumpPower = defaultJumpPower;
                lastDefaultJumpPower = defaultJumpPower;
            }
        }

        private void FixedUpdate()
        {    
            DoRiseResistance();
            DoFallMultiplier();
        }

        public void StartJump()
        {
            if (groundDetect.IsGrounded)
            {
                Jump();
            }
            else if (curInAirJumps < inAirJumps)
            {
                SleepVertical();
                Jump();
                curInAirJumps++;
            }
            
        }

        private void Jump()
        {
            rb.AddForce(transform.up * curJumpPower, forceMode);
            IsJumping = true;
        }

        private void SleepVertical()
        {
            rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
        }

        public void StopJump()
        {
            IsJumping = false;
        }

        public void SetJumpPower(float power)
        {
            curJumpPower = power;
        }

        private void DoRiseResistance()
        {
            if (groundDetect.IsGrounded) return;
            if (disableRiseResistanceOnJump && IsJumping) return;

            if (rb.velocity.y > 0 && riseResistance > 0)
            {
                rb.AddForce(-transform.up * riseResistance, forceMode);
            }
        }

        private void DoFallMultiplier()
        {
            if (groundDetect.IsGrounded) return;

            if (rb.velocity.y < fallStartOnYVelocity && fallMultiplier > 0)
            {
                rb.AddForce(-transform.up * fallMultiplier, forceMode);
            }
        }
    }
}
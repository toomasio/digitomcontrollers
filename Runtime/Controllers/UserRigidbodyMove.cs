﻿using System.Collections;
using System.Collections.Generic;
using DigitomPhysics;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DigitomControllers
{
    [RequireComponent(typeof(UserInputMove))]
    public class UserRigidbodyMove : RigidbodyMoveStateMachine
    {
        protected UserInputMove input;

        protected override void Awake()
        {
            base.Awake();
            input = GetComponent<UserInputMove>();
        }

        protected override void Update()
        {
            if (input)
                CurDirectionInput = input.InputValue;

            base.Update();
        }
    }
}
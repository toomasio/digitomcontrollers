﻿using System.Collections;
using System.Collections.Generic;
using DigitomPhysics;
using DigitomUtilities;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DigitomControllers
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(DetectGrounded))]
    public class UserRigidbodyFreeLookController : MonoBehaviour
    {
        [SerializeField] private InputActionReference moveActionRef;
        [SerializeField] private InputActionReference lookActionRef;
        [SerializeField] private InputActionReference jumpActionRef;
        [SerializeField] private InputActionReference crouchActionRef;
        [SerializeField] private Transform lookTransform;
        [SerializeField] private float rotationSpeed = 5f;
        [SerializeField] private float lookSensitivity = 0.1f;
        [SerializeField] private bool invertLook = false;
        [SerializeField] private RigidbodyMoveData moveData;
        [SerializeField] private RigidbodyDragData groundedDrag;
        [SerializeField] private MoveInputProcessorMultiplierData groundedSpeed;
        [SerializeField] private RigidbodyDragData inAirDrag;
        [SerializeField] private MoveInputProcessorMultiplierData inAirSpeed;
        [SerializeField] private RigidbodyGravity3DSettings inAirGravity;
        [SerializeField] private JumpSettings jumpData;
        [SerializeField] private CrouchSettings crouchSettings;
        [SerializeField] private MoveInputProcessorCrouchData crouchSpeedData;
        [SerializeField] private MoveInputProcessorSlopeData slope;
        [SerializeField] private PlatformStickSettings platformStickData;

        private Vector3 moveInput;
        public Vector3 MoveInput { set => moveInput = value; }
        private Vector3 lookInput;
        public Vector3 LookInput { set => lookInput = value; }

        private Rigidbody rb;
        private DetectGrounded grounded;

        private RigidbodyDragSystem groundedDragSystem;
        private RigidbodyDragSystem inAirDragSystem;
        private MoveInputProcessorMultiplierSystem groundedSpeedSystem;
        private MoveInputProcessorMultiplierSystem inAirSpeedSystem;
        private RigidbodyGravity3DSystem inAirGravitySystem;

        private MoveInputProcessorCalculatorSystem moveProcessor;
        private MoveInputProcessorMapperSystem moveMapper;
        private MoveInputProcessorSpaceSystem moveSpace;
        private MoveInputProcessorCrouchSystem moveCrouch;
        private MoveInputProcessorSlopeSystem slopeSystem;

        private RigidbodyMoveSystem moveSystem;
        private JumpSystem3D jumpSystem;
        private CrouchSystem3D crouchSystem;

        private PlatformStickSystem stickSystem;

        private PlayerInput playerInput;
        private InputAction moveAction;
        private InputAction lookAction;
        private InputAction jumpAction;
        private InputAction crouchAction;
        private float lookX;
        private float lookY;

        private bool jumpTriggered;
        private Timer crouchHoldTimer;

        private void Awake()
        {
            //get comps
            rb = GetComponent<Rigidbody>();
            grounded = GetComponent<DetectGrounded>();
            playerInput = GetComponent<PlayerInput>();

            //get user inputs
            moveAction = playerInput.actions.FindAction(moveActionRef.action.id);
            lookAction = playerInput.actions.FindAction(lookActionRef.action.id);
            jumpAction = playerInput.actions.FindAction(jumpActionRef.action.id);
            crouchAction = playerInput.actions.FindAction(crouchActionRef.action.id);

            //speed and drag
            groundedDragSystem = new RigidbodyDragSystem(rb, groundedDrag);
            inAirDragSystem = new RigidbodyDragSystem(rb, inAirDrag);
            groundedSpeedSystem = new MoveInputProcessorMultiplierSystem(groundedSpeed);
            inAirSpeedSystem = new MoveInputProcessorMultiplierSystem(inAirSpeed);

            //gravity
            inAirGravitySystem = new RigidbodyGravity3DSystem(inAirGravity, rb, grounded.GroundDetection);

            //move processing
            moveProcessor = new MoveInputProcessorCalculatorSystem();
            moveMapper = new MoveInputProcessorMapperSystem(new MoveInputProcessorMappingData
            {
                mapXTo = AxisType.X,
                mapYTo = AxisType.Z,
                mapZTo = AxisType.None
            });
            moveProcessor.AddProcessor(moveMapper);
            moveSpace = new MoveInputProcessorSpaceSystem(transform, new MoveInputProcessorSpaceData { space = DirectionSpace.Camera });
            moveProcessor.AddProcessor(moveSpace);
            moveCrouch = new MoveInputProcessorCrouchSystem(crouchSpeedData);
            moveProcessor.AddProcessor(moveCrouch);
            slopeSystem = new MoveInputProcessorSlopeSystem(transform, grounded.GroundDetection, slope);
            moveProcessor.AddProcessor(slopeSystem);
            moveProcessor.AddProcessor(groundedSpeedSystem);
            moveProcessor.AddProcessor(inAirSpeedSystem);

            //move system
            moveSystem = new RigidbodyMoveSystem(rb, moveData);

            //jump system
            jumpSystem = new JumpSystem3D(rb, grounded.GroundDetection, jumpData);

            //crouch system
            crouchSystem = new CrouchSystem3D(grounded.GroundDetection, crouchSettings, transform, GetComponent<CapsuleCollider>());

            //stick system
            stickSystem = new PlatformStickSystem(platformStickData, grounded);
        }

        private void Update()
        {
            //grounded / in-air switching
            groundedDragSystem.Enabled = grounded.IsGrounded;
            inAirDragSystem.Enabled = !grounded.IsGrounded;
            groundedSpeedSystem.Enabled = grounded.IsGrounded;
            inAirSpeedSystem.Enabled = !grounded.IsGrounded;
            moveCrouch.Enabled = crouchSystem.State != CrouchSystem3D.BodyState.Standing;

            //drag ticks
            groundedDragSystem.Tick();
            inAirDragSystem.Tick();

            //set move inputs
            MoveInput = moveAction.ReadValue<Vector2>();
            moveMapper.Input = moveInput;

            //set look input control
            LookInput = lookAction.ReadValue<Vector2>();
            lookX += lookInput.x * lookSensitivity;
            lookY += invertLook ? lookInput.y * lookSensitivity : -lookInput.y * lookSensitivity;
            lookY = Mathf.Clamp(lookY, -85, 85);

            //do look
            lookTransform.localRotation = Quaternion.Euler(new Vector3(lookY, 0, 0));

            //do rotation
            var moveRot = moveProcessor.Output;
            moveRot.y = 0;
            if (moveRot != Vector3.zero)
            {
                var targetRotation = Quaternion.LookRotation(moveRot, Vector3.up);
                rb.rotation = Quaternion.Slerp(rb.rotation, targetRotation, Time.deltaTime * rotationSpeed);
            }
            
            //do jump
            if (jumpAction.triggered)
                jumpTriggered = true;

            //do crouch
            if (crouchAction.triggered)
            {
                if (crouchSystem.State == CrouchSystem3D.BodyState.Standing ||
                    crouchSystem.State == CrouchSystem3D.BodyState.Proneing)
                    crouchSystem.Crouch();
                else if (crouchSystem.State == CrouchSystem3D.BodyState.Crouching)
                    crouchSystem.Stand();
            }
            //hold crouch to transition from standing to prone
            if (crouchAction.IsPressed() && crouchSystem.State == CrouchSystem3D.BodyState.Crouching
                && crouchSystem.PrevState == CrouchSystem3D.BodyState.Standing)
            {
                crouchHoldTimer.Tick();
                if (crouchHoldTimer.Perc >= 1)
                    crouchSystem.Prone();
            }
            else
                crouchHoldTimer = new Timer(0.1f);

            crouchSystem.Tick();
        }

        private void FixedUpdate()
        {
            //jump
            if (jumpTriggered)
            {
                jumpSystem.OnTriggered();
                jumpTriggered = false;
            }

            //movement processing
            moveMapper.Tick();
            moveSpace.Tick();
            moveCrouch.Tick();
            slopeSystem.Tick();
            groundedSpeedSystem.Tick();
            inAirSpeedSystem.Tick();

            //gravity
            inAirGravitySystem.Tick();

            //final move processing Calculation
            moveProcessor.Tick();

            //stick
            stickSystem.Tick();

            //movement
            moveSystem.Input = moveProcessor.Output;
            moveSystem.Tick();
        }

        private void OnDrawGizmosSelected()
        {
            if (moveMapper != null)
            {
                var offset = Vector3.up * 0.01f;
                var startPos = transform.position + offset;
                var endPos = startPos + moveProcessor.Output;
                Gizmos.DrawLine(startPos, endPos);
            }

        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using DigitomPhysics;
using UnityEngine;

namespace DigitomControllers
{
    [RequireComponent(typeof(Rigidbody))]
    public class RigidbodyMoveStateMachine : MonoMoveStateMachine
    {
        protected Rigidbody rb;

        protected override void Awake()
        {
            rb = GetComponent<Rigidbody>();
            base.Awake();
        }

        protected override IMoveState GetGroundedState()
        {
            return new RigidbodyMovementSystem(transform, rb, groundedMovement);
        }

        protected override IMoveState GetInAirState()
        {
            return new RigidbodyMovementSystem(transform,rb, inAirMovement);
        }
    }
}
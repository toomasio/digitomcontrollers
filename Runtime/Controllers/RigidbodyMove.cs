﻿using System.Collections;
using System.Collections.Generic;
using DigitomPhysics;
using UnityEngine;

namespace DigitomControllers
{
    [RequireComponent(typeof(Rigidbody), typeof(InputMoveCalculator))]
    public class RigidbodyMove : MonoBehaviour
    {
        [SerializeField] protected float defaultSpeed = 5;
        [SerializeField] protected ForceMode forceMode = default;

        protected float curSpeed;
        protected Rigidbody rb;
        protected InputMoveCalculator input;

        protected void Awake()
        {
            rb = GetComponent<Rigidbody>();
            input = GetComponent<InputMoveCalculator>();
            curSpeed = defaultSpeed;
        }

        protected void FixedUpdate()
        {
            var force = input.OutputDirection * curSpeed;
            rb.AddForce(force, forceMode);
        }

        public void SetSpeed(float speed)
        {
            curSpeed = speed;
        }
    }
}
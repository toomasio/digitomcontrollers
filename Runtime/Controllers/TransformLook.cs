﻿using System.Collections;
using System.Collections.Generic;
using DigitomPhysics;
using UnityEngine;

namespace DigitomControllers
{
    public class TransformLook : MonoBehaviour
    {
        [SerializeField] private float startSpeedMultiplier = 1;
        [SerializeField] private Vector2 startDirectionInput = Vector2.zero;
        [SerializeField] private LookSettings groundedLook = default;
        private LookStateMachine stateMachine;
        private TransformLookSystem groundLook;

        public Vector2 CurDirectionInput { get; set; }
        public float CurSpeedMultiplier { get; set; }

        public bool Grounded { get; private set; }

        protected virtual void Awake()
        {
            CurSpeedMultiplier = startSpeedMultiplier;
            CurDirectionInput = startDirectionInput;
            stateMachine = new LookStateMachine();
            groundLook = new TransformLookSystem(transform, groundedLook);
            stateMachine.SetState(groundLook);
        }

        protected virtual void Start()
        {
            Grounded = true;
        }

        protected virtual void OnEnable() { }

        protected virtual void OnDisable() { }

        protected virtual void Update()
        {
            stateMachine.DirectionInput = CurDirectionInput;
            stateMachine.SpeedMultiplier = CurSpeedMultiplier;
            stateMachine.Tick();
        }

        public virtual void SetXInput(float xValue)
        {
            CurDirectionInput = new Vector3(xValue, CurDirectionInput.y);
        }

        public virtual void SetYInput(float yValue)
        {
            CurDirectionInput = new Vector3(CurDirectionInput.x, yValue);
        }

        public virtual void FlipInputDirection()
        {
            CurDirectionInput = -CurDirectionInput;
        }

    }
}
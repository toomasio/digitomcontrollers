﻿using System.Collections;
using System.Collections.Generic;
using DigitomPhysics;
using UnityEngine;

namespace DigitomControllers
{
    [RequireComponent(typeof(OverlapBoxDetection))]
    public abstract class MonoMoveStateMachine : MonoBehaviour
    {
        [SerializeField] protected float startSpeedMultiplier = 1;
        [SerializeField] protected Vector2 startDirectionInput = Vector2.zero;
        [SerializeField] protected string groundDetectName = "Feet";
        [SerializeField] protected MovementSettings groundedMovement = default;
        [SerializeField] protected MovementSettings inAirMovement = default;
        protected MoveStateMachine stateMachine;
        protected IMoveState groundMove;
        protected IMoveState inAirMove;
        protected abstract IMoveState GetGroundedState();
        protected abstract IMoveState GetInAirState();

        public Vector2 CurDirectionInput { get; set; }
        public Vector3 CurDesiredVelocity
        {
            get
            {
                if (stateMachine == null) return default;
                if (stateMachine.CurState == null) return default;
                return stateMachine.CurState.CurDesiredVelocity;
            }
        }
        public float CurSpeedMultiplier { get; set; }
        public bool Grounded { get; private set; }
        protected OverlapBoxDetection physicsDetection;
        protected IPhysicsDetectable3D groundedSystem;

        protected virtual void Awake()
        {
            physicsDetection = GetComponent<OverlapBoxDetection>();
            CurSpeedMultiplier = startSpeedMultiplier;
            CurDirectionInput = startDirectionInput;
            stateMachine = new MoveStateMachine();
            groundMove = GetGroundedState();
            inAirMove = GetInAirState();
            stateMachine.AddTransition(groundMove, () => Grounded);
            stateMachine.AddTransition(inAirMove, () => !Grounded);
        }

        protected virtual void Start()
        {
            groundedSystem = physicsDetection.GetSystem(groundDetectName);
            if (groundedSystem != null)
            {
                groundedSystem.OnFirst += OnGrounded;
                groundedSystem.OnEmpty += OnInAir;
            }

        }

        protected virtual void OnEnable()
        {
            if (groundedSystem == null)return;
            groundedSystem.OnFirst += OnGrounded;
            groundedSystem.OnEmpty += OnInAir;
        }

        protected virtual void OnDisable()
        {
            if (groundedSystem != null)
            {
                groundedSystem.OnFirst += OnGrounded;
                groundedSystem.OnEmpty += OnInAir;
            }

        }

        protected virtual void Update()
        {
            stateMachine.DirectionInput = CurDirectionInput;
            stateMachine.SpeedMultiplier = CurSpeedMultiplier;
            stateMachine.Tick();
        }

        protected virtual void FixedUpdate()
        {
            stateMachine.FixedTick();
        }

        protected virtual void OnDrawGizmosSelected()
        {
            stateMachine?.DrawGizmos();
        }

        protected virtual void OnGrounded(HitInfo hitInfo)
        {
            Grounded = true;
        }

        protected virtual void OnInAir(Collider col)
        {
            Grounded = false;
        }

        public virtual void SetXInput(float xValue)
        {
            CurDirectionInput = new Vector3(xValue, CurDirectionInput.y);
        }

        public virtual void SetYInput(float yValue)
        {
            CurDirectionInput = new Vector3(CurDirectionInput.x, yValue);
        }

        public virtual void FlipInputDirection()
        {
            CurDirectionInput = -CurDirectionInput;
        }

    }
}
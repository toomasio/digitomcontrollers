﻿using System.Collections;
using System.Collections.Generic;
using DigitomPhysics;
using UnityEngine;

namespace DigitomControllers
{
    [RequireComponent(typeof(TransformVelocity))]
    public class TransformRotateTowardsVelocity : MonoBehaviour
    {
        [SerializeField] private float rotateSpeed = 1;
        [SerializeField] private bool ignoreY = true;

        private TransformVelocity velocity;

        protected virtual void Awake()
        {
            velocity = GetComponent<TransformVelocity>();
        }

        private void FixedUpdate()
        {
            if (velocity == null) return;
            var vel = velocity.VelocityDirection;
            if (ignoreY)
                vel.y = 0;
            if (vel == default) return;
            var desiredRot = Quaternion.LookRotation(vel);
            transform.rotation = Quaternion.Slerp(transform.rotation, desiredRot, rotateSpeed * Time.deltaTime);
        }

    }
}
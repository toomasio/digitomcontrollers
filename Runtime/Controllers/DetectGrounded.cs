﻿using DigitomPhysics;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitomUtilities;

namespace DigitomControllers
{
    public class DetectGrounded : MonoBehaviour
    {
        [SerializeField] protected Transform detectFrom;
        [SerializeField] protected SphereCastSettings groundedSettings;

        public bool IsGrounded { get; protected set; }
        public SphereCast GroundDetection 
        {
            get 
            {
                if (detection == null) 
                    detection = new SphereCast(detectFrom, groundedSettings);
                return detection;
            } 
        }
        protected SphereCast detection;

        public Action<Collider> OnGrounded;
        public Action<Collider> OnNotGrounded;

        private void OnEnable()
        {
            if (detection == null)
                detection = new SphereCast(detectFrom, groundedSettings);
            detection.OnFirst += Grounded;
            detection.OnEmpty += NotGrounded;
        }

        private void OnDisable()
        {
            detection.OnFirst -= Grounded;
            detection.OnEmpty -= NotGrounded;
        }

        private void OnDrawGizmos()
        {
            GroundDetection.DrawDetectable();
            if (!Application.isPlaying)
                GroundDetection.Tick();
        }

        private void FixedUpdate()
        {
            if (detection == null) return;
            detection.Tick();
        }

        private void Grounded(HitInfo hitInfo)
        {
            IsGrounded = true;
            OnGrounded?.Invoke(hitInfo.collider);
        }

        private void NotGrounded(Collider col)
        {
            IsGrounded = false;
            OnNotGrounded?.Invoke(col);
        }

        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using DigitomPhysics;
using UnityEngine;

namespace DigitomControllers
{
    public class TransformMove : MonoMoveStateMachine
    {
        protected override IMoveState GetGroundedState()
        {
            return new TransformMovementSystem(transform, groundedMovement);
        }

        protected override IMoveState GetInAirState()
        {
            return new TransformMovementSystem(transform, inAirMovement);
        }
    }
}
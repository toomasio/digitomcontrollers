﻿using System.Collections;
using System.Collections.Generic;
using DigitomPhysics;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DigitomControllers
{
    [RequireComponent(typeof(PlayerInput))]
    public class UserTransMove : TransformMove
    {
        [SerializeField] private InputActionReference moveInput = null;
        private InputAction moveInputAction;
        private PlayerInput input;

        protected override void Awake()
        {
            base.Awake();
            input = GetComponent<PlayerInput>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            if (moveInput != null)
            {
                moveInputAction = input.actions.FindAction(moveInput.action.id);
                moveInputAction.Enable();
                moveInputAction.performed += OnInputMove;
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            if (moveInput != null)
                moveInputAction.performed -= OnInputMove;
        }

        void OnInputMove(InputAction.CallbackContext ctx)
        {
            CurDirectionInput = ctx.ReadValue<Vector2>();
        }
    }
}
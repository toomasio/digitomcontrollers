﻿using System;
using System.Collections;
using System.Collections.Generic;
using DigitomPhysics;
using DigitomUtilities;
using UnityEngine;
using UnityEngine.Events;

namespace DigitomControllers
{
    public class MoveToPoint : MonoBehaviour
    {
        private enum MoveType { Speed, Time }
        private enum UpdateType { Update, FixedUpdate, LateUpdate}
        [SerializeField] private Vector3 targetPos = Vector3.zero;
        [SerializeField] private MoveType moveType = MoveType.Speed;
        [SerializeField] private float speed = 5;
        [SerializeField] private float time = 1;
        [SerializeField] private bool moveOnStart = false;
        [SerializeField] private bool interruptable = false;
        [SerializeField] private UpdateType updateType = UpdateType.Update;
        [SerializeField] private UnityEvent onTargetPosReached = null;
        [SerializeField] private UnityEvent onStartPosReached = null;

        private Vector3 startPos;
        private float moveTimer;
        private float moveTime;
        private float movePerc;
        private bool moving;
        private bool waitingToMove;
        private Vector3 curWaitTargetPos;
        private Vector3 curStartPos;
        private Vector3 curTargetPos;
        private Action curAction;
        private Action curWaitAction;

        private void Start()
        {
            startPos = transform.localPosition;
            if (moveOnStart)
                MoveToTargetPos();
        }

        private void Update()
        {
            if (updateType != UpdateType.Update) return;
            CheckWaitToMove();
            DoMoveToPosition();
        }

        private void FixedUpdate()
        {
            if (updateType != UpdateType.FixedUpdate) return;
            CheckWaitToMove();
            DoMoveToPosition();
        }

        private void LateUpdate()
        {
            if (updateType != UpdateType.LateUpdate) return;
            CheckWaitToMove();
            DoMoveToPosition();
        }

        public void MoveToTargetPos()
        {
            MoveToPos(targetPos, OnTargetPositionReached);
        }

        public void MoveToStartPos()
        {
            MoveToPos(startPos, OnStartPositionReached);
        }

        private void OnStartPositionReached()
        {
            onStartPosReached?.Invoke();
        }

        private void OnTargetPositionReached()
        {
            onTargetPosReached?.Invoke();
        }

        private void MoveToPos(Vector3 pos, Action callback = null)
        {
            if (moving)
            {
                if (interruptable)
                {
                    ResetMove();
                    CalculateMovement(pos);
                }
                else
                {
                    waitingToMove = true;
                    curWaitTargetPos = pos;
                    curWaitAction = callback;
                    return;
                }
                    
            }

            ResetMove();
            CalculateMovement(pos);
            curAction = callback;
            moving = true;
        }

        private void DoMoveToPosition()
        {
            if (!moving) return;

            moveTimer += Time.deltaTime;
            if (moveTimer > moveTime)
                moveTimer = moveTime;
            movePerc = moveTimer / moveTime;

            var lerp = Vector3.Lerp(curStartPos, curTargetPos, movePerc);
            if (!lerp.IsNaN())
                transform.localPosition = lerp;

            if (movePerc == 1)
            {
                transform.localPosition = curTargetPos;
                curAction.Invoke();
                StopMovement();
            }
        }

        private void CheckWaitToMove()
        {
            if (!waitingToMove) return;
            if (moving) return;

            ResetMove();
            MoveToPos(curWaitTargetPos, curWaitAction);
            StopWaitToMove();
        }

        private void ResetMove()
        {
            moveTimer = 0;
        }

        private void CalculateMovement(Vector3 target)
        {
            curStartPos = transform.localPosition;
            curTargetPos = target;
            float dist = Vector3.Distance(curStartPos, curTargetPos);
            moveTime = dist / speed;
        }

        private void StopMovement()
        {
            moving = false;
            curAction = null;
            curStartPos = default;
            curTargetPos = default;
        }

        private void StopWaitToMove()
        {
            curWaitAction = null;
            curWaitTargetPos = default;
            waitingToMove = false;
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using DigitomPhysics;
using UnityEngine;

namespace DigitomControllers
{
    public class RotateTowardsMoveOutput : MonoBehaviour
    {
        [SerializeField] private float rotateSpeed = 1;
        [SerializeField] private bool ignoreY = true;

        private InputMoveCalculator input;

        protected virtual void Awake()
        {
            input = GetComponent<InputMoveCalculator>();
        }

        private void FixedUpdate()
        {
            if (input == null) return;
            var vel = input.OutputDirection;
            if (ignoreY)
                vel.y = 0;
            if (vel == default) return;
            var desiredRot = Quaternion.LookRotation(vel);
            transform.rotation = Quaternion.Slerp(transform.rotation, desiredRot, rotateSpeed * Time.deltaTime);
        }

    }
}
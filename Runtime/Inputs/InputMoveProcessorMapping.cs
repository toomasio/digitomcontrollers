﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitomUtilities;

namespace DigitomControllers
{
    [RequireComponent(typeof(InputMoveCalculator))]
    public class InputMoveProcessorMapping : MonoBehaviour, IMoveInputProcessor
    {
        [SerializeField] protected AxisType inputXTo = AxisType.X;
        [SerializeField] protected AxisType inputYTo = AxisType.Z;
        [SerializeField] protected AxisType inputZTo = AxisType.None;

        public Vector3 InputDirection { get; set; }
        public Vector3 OutputDirection
        {
            get
            {
                var convertedMove = InputDirection;
                //x input
                if (inputXTo == AxisType.X)
                    convertedMove = new Vector3(InputDirection.x, convertedMove.y, convertedMove.z);
                else if (inputXTo == AxisType.Y)
                    convertedMove = new Vector3(convertedMove.x, InputDirection.x, convertedMove.z);
                else if (inputXTo == AxisType.Z)
                    convertedMove = new Vector3(convertedMove.x, convertedMove.y, InputDirection.x);

                //y input
                if (inputYTo == AxisType.X)
                    convertedMove = new Vector3(InputDirection.y, convertedMove.y, convertedMove.z);
                else if (inputYTo == AxisType.Y)
                    convertedMove = new Vector3(convertedMove.x, InputDirection.y, convertedMove.z);
                else if (inputYTo == AxisType.Z)
                    convertedMove = new Vector3(convertedMove.x, convertedMove.y, InputDirection.y);

                //z input
                if (inputZTo == AxisType.X)
                    convertedMove = new Vector3(InputDirection.z, convertedMove.y, convertedMove.z);
                else if (inputZTo == AxisType.Y)
                    convertedMove = new Vector3(convertedMove.x, InputDirection.z, convertedMove.z);
                else if (inputZTo == AxisType.Z)
                    convertedMove = new Vector3(convertedMove.x, convertedMove.y, InputDirection.z);

                return convertedMove;
            } 
        }
    }
}



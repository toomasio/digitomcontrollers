﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitomUtilities;
using DigitomPhysics;

namespace DigitomControllers
{
    [RequireComponent(typeof(DetectGrounded))]
    public class InputMoveProcessorSlopeDetection : MonoBehaviour, IMoveInputProcessor
    {
        [SerializeField] private bool enableMaxSlope;
        [SerializeField] private float maxSlopeAngle = 30;

        protected DetectGrounded detectGrounded;
        protected IPhysicsDetectable3D groundSystem;

        private void Awake()
        {
            detectGrounded = GetComponent<DetectGrounded>();
        }

        public Vector3 InputDirection { get; set; }
        public Vector3 OutputDirection
        {
            get
            {
                var convertedMove = InputDirection;
                if (groundSystem == null)
                {
                    groundSystem = detectGrounded.GroundDetection;
                    return convertedMove;
                }
                
                if (groundSystem.Detected && InputDirection != default)
                {
                    //need to find the closest hit position relative to input direction...then pick that hitnormal
                    //this should prevent the player getting stuck when colliding with multiple surfaces
                    float highestDot = 0;
                    var closestNorm = groundSystem.HitNormal;
                    for (int i = 0; i < groundSystem.HitInfos.Length; i++)
                    {
                        var pos = groundSystem.HitInfos[i].hitPoint;
                        var norm = groundSystem.HitInfos[i].hitNormal;
                        var dir = (pos - transform.position);
                        dir.y = 0;
                        dir = dir.normalized;
                        var angle = Vector3.Dot(InputDirection.normalized, dir);
                        if (angle > highestDot)
                        {                            
                            closestNorm = norm;
                            highestDot = angle;
                        }
                            
                    }
                    convertedMove = Vector3.ProjectOnPlane(convertedMove, closestNorm);

                    if (enableMaxSlope)
                    {
                        var angle = Vector3.Angle(Vector3.up, closestNorm);
                        
                        if (angle > maxSlopeAngle && convertedMove.y > 0)
                        {
                            convertedMove = Vector3.zero;
                        }
                    }
                        
                }
                return convertedMove;
            } 
        }
    }
}



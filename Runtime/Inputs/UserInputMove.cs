﻿using System;
using System.Collections;
using System.Collections.Generic;
using DigitomInputs;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DigitomControllers
{
    public class UserInputMove : UserInputAction<Vector2>, IMoveInput
    {
        public event Action<Vector3> OnInputReceived;
        public Vector3 MoveInput { get; protected set; }

        protected override void OnInputPerformed(InputAction.CallbackContext ctx)
        {
            base.OnInputPerformed(ctx);
            MoveInput = InputValue;
            OnInputReceived?.Invoke(MoveInput);
        }
    }
}
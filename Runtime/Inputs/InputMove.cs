﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitomUtilities;

namespace DigitomControllers
{
    public class InputMove : MonoBehaviour
    {
        [SerializeField] protected AxisType inputXTo = AxisType.X;
        [SerializeField] protected AxisType inputYTo = AxisType.Z;
        [SerializeField] protected AxisType inputZTo = AxisType.None;
        [SerializeField] protected AxisType positionLock = AxisType.None;
        [SerializeField] protected StateMode holdPositionLock = StateMode.None;
        [SerializeField] protected AxisType flattenAxis = AxisType.None;
        [SerializeField] protected DirectionSpace space = DirectionSpace.Self;
        [SerializeField] protected bool normalizeOutput = false;
        protected Camera cam;

        public Vector3 InputDirection { get; set; }
        public Vector3 OutputDirection
        {
            get
            {
                var convertedMove = new Vector3();
                //x input
                if (inputXTo == AxisType.X)
                    convertedMove = new Vector3(InputDirection.x, convertedMove.y, convertedMove.z);
                else if (inputXTo == AxisType.Y)
                    convertedMove = new Vector3(convertedMove.x, InputDirection.x, convertedMove.z);
                else if (inputXTo == AxisType.Z)
                    convertedMove = new Vector3(convertedMove.x, convertedMove.y, InputDirection.x);

                //y input
                if (inputYTo == AxisType.X)
                    convertedMove = new Vector3(InputDirection.y, convertedMove.y, convertedMove.z);
                else if (inputYTo == AxisType.Y)
                    convertedMove = new Vector3(convertedMove.x, InputDirection.y, convertedMove.z);
                else if (inputYTo == AxisType.Z)
                    convertedMove = new Vector3(convertedMove.x, convertedMove.y, InputDirection.y);

                //z input
                if (inputZTo == AxisType.X)
                    convertedMove = new Vector3(InputDirection.z, convertedMove.y, convertedMove.z);
                else if (inputZTo == AxisType.Y)
                    convertedMove = new Vector3(convertedMove.x, InputDirection.z, convertedMove.z);
                else if (inputZTo == AxisType.Z)
                    convertedMove = new Vector3(convertedMove.x, convertedMove.y, InputDirection.z);

                //spacial direction
                if (space == DirectionSpace.Self)
                    convertedMove = transform.TransformDirection(convertedMove);
                else if (space == DirectionSpace.Camera)
                {
                    if (!cam) cam = Camera.main;
                    convertedMove = cam.transform.TransformDirection(convertedMove);
                    convertedMove = Vector3.ProjectOnPlane(convertedMove, transform.up);
                }

                //ignore certain axis?
                if (flattenAxis == AxisType.X)
                    convertedMove.x = 0;
                else if (flattenAxis == AxisType.Y)
                    convertedMove.y = 0;
                else if (flattenAxis == AxisType.Z)
                    convertedMove.z = 0;

                if (normalizeOutput)
                    convertedMove = convertedMove.normalized;

                return convertedMove;
            } 
        }
    }
}



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitomUtilities;

namespace DigitomControllers
{
    public class InputMoveProcessorSpace : MonoBehaviour, IMoveInputProcessor
    {
        [SerializeField] protected DirectionSpace space = DirectionSpace.Self;
        protected Camera cam;

        public Vector3 InputDirection { get; set; }
        public Vector3 OutputDirection
        {
            get
            {
                var convertedMove = InputDirection;

                //spacial direction
                if (space == DirectionSpace.Self)
                    convertedMove = transform.TransformDirection(convertedMove);
                else if (space == DirectionSpace.Camera)
                {
                    if (!cam) cam = Camera.main;
                    var dir = transform.position - cam.transform.position;
                    dir.y = 0;
                    dir = dir.normalized;
                    var matrix = Matrix4x4.TRS(transform.position, Quaternion.LookRotation(dir), Vector3.one);
                    convertedMove = matrix.MultiplyVector(convertedMove);
                    //convertedMove = cam.transform.TransformDirection(convertedMove);
                    //convertedMove = Vector3.ProjectOnPlane(convertedMove, transform.up);
                    
                    //Debug.Log(convertedMove);
                }

                return convertedMove;
            } 
        }
    }
}



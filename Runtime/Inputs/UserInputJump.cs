﻿using System;
using System.Collections;
using System.Collections.Generic;
using DigitomInputs;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DigitomControllers
{
    public class UserInputJump : UserInputAction<bool>, IJumpInput
    {
        public event Action OnInpPerformed;
        public event Action OnInpCanceled;

        protected override void OnInputPerformed(InputAction.CallbackContext ctx)
        {
            OnInpPerformed?.Invoke();
        }

        protected override void OnInputCanceled(InputAction.CallbackContext ctx)
        {
            OnInpCanceled?.Invoke();
        }
    }
}
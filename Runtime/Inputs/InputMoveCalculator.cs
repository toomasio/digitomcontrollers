﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitomUtilities;
using System;

namespace DigitomControllers
{
    [RequireComponent(typeof(IMoveInput))]
    public class InputMoveCalculator : MonoBehaviour
    {
        [SerializeField, ComponentOfType(typeof(IMoveInputProcessor))] protected Component[] processors;
        protected IMoveInputProcessor[] convertedProcessors;

        public Vector3 InputDirection { get; private set; }
        public Vector3 OutputDirection { get; private set; }
        protected IMoveInput move;

        public event Action<Vector3> OnOutputDirectionCalculated;

        private void Awake()
        {
            convertedProcessors = new IMoveInputProcessor[processors.Length];
            for (int i = 0; i < processors.Length; i++)
            {
                convertedProcessors[i] = processors[i] as IMoveInputProcessor;
            }

            move = GetComponent<IMoveInput>();
        }

        private void OnEnable()
        {
            move.OnInputReceived += CalculateOutputDirection;
        }
        private void OnDisable()
        {
            move.OnInputReceived -= CalculateOutputDirection;
        }

        private void CalculateOutputDirection(Vector3 input)
        {
            InputDirection = input;

            OutputDirection = Vector3.zero;
            if (convertedProcessors == null) return;
            for (int i = 0; i < convertedProcessors.Length; i++)
            {
                var proc = convertedProcessors[i];
                proc.InputDirection = i == 0 ? InputDirection : OutputDirection;
                OutputDirection = convertedProcessors[i].OutputDirection;
            }

            OnOutputDirectionCalculated?.Invoke(OutputDirection);
        }

        public void OnDrawGizmosSelected()
        {
            if (OutputDirection == default) return;
            Gizmos.color = Color.cyan;
            Gizmos.DrawSphere(transform.position + OutputDirection, 0.1f);
        }
    }
}



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitomUtilities;

namespace DigitomControllers
{
    public class TransformVelocity : MonoBehaviour
    {
        [SerializeField] private MonoUpdateType updateType = default;

        public Vector3 Velocity { get; private set; }
        public Vector3 VelocityDirection { get; private set; }
        public float Speed { get; private set; }
        private Vector3 lastPos;

        private void Awake()
        {
            lastPos = transform.position;
        }

        private void Update()
        {
            if (updateType != MonoUpdateType.Update) return;
            CalculateVelocity();
        }

        private void FixedUpdate()
        {
            if (updateType != MonoUpdateType.FixedUpdate) return;
            CalculateVelocity();
        }

        private void LateUpdate()
        {
            if (updateType != MonoUpdateType.LateUpdate) return;
            CalculateVelocity();
        }

        private void CalculateVelocity()
        {
            Velocity = transform.position - lastPos;
            VelocityDirection = Velocity.normalized;
            Speed = Mathf.Round((Velocity / Time.deltaTime).magnitude * 100f) / 100f;
            lastPos = transform.position;
        }
    }
}


